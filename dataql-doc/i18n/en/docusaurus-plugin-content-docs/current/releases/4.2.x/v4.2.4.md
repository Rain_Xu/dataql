---
id: v4.2.4
title: v4.2.4 (2021-03-17)
---

# v4.2.4 (2021-03-17)

## 优化
- 优化 [Issue I396OL](https://gitee.com/zycgit/hasor/issues/I396OL) lambda 函数中必须要有 return 否则会报错。
- 优化 responseFormat 部分在异常的时候 message 中代码所处行号等信息，单独拆分到 location 中。
