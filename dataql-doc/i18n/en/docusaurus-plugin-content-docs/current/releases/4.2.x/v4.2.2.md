---
id: v4.2.2
title: v4.2.2 (2021-01-20)
---

# v4.2.2 (2021-01-20)

## 新增
- 新增登陆页面，Admin 页面不在裸奔了。开发者可以自己扩展登陆逻辑，包括页面也可以自定义。
- Dataway 新增达梦数据库的脚本。
- [Issue I1XR17](https://gitee.com/zycgit/hasor/issues/I1XR17) 可以通过开关来关闭 Git 按钮的展示功能，同时优化 git 挂件的加载。
- [Issue 83](https://github.com/zycgit/hasor/issues/83) 增加多数据源下，使用数据库存储 Dataway 接口信息时，元信息独立存储 DataQL 无法直接访问它，增加安全性
- [Issue 81](https://github.com/zycgit/hasor/issues/81) 支持通过配置 `HASOR_DATAQL_DATAWAY_DB_TABLE_PREFIX` 来指定两张表的表名前缀
- [Issue I1SMM2](https://gitee.com/zycgit/hasor/issues/I1SMM2) 增加可以配置方式开启或者关闭跨域能力，同时界面上也可以为每一个 API 单独进行跨域功能的配置。
- `NacosApiDataAccessLayer` 可以脱离 `nacos-spring-boot-starter` 来独立使用。
- 新增 spring cloud dataway with alibaba nacos 的支持，例子下载 [点击这里](https://gitee.com/zycgit/hasor/tree/master/example/springcloud-dataway-nacos)
- 新增 `HASOR_DATAQL_DATAWAY_NACOSDAL_API_MAX_SIZE` 配置项。
- dataql-fx 增加去重函数。`CollectionUdfSource.uniqueBy`。
- [PR 16](https://gitee.com/zycgit/hasor/pulls/16) 增加 `@@mybatis` 对 `<trim>`、`<set>`、`<where>` 标签的支持（小牧zzz <13507502@qq.com>）

## 优化
- Dataway 调整 h2 数据库脚本格式。
- DeleteController、DisableController 的能力在 DatawayService 接口上同时提供。
- dataway 和 springboot 的样板工程优化。
- Dataway 前后台的数据传输处理中不再强制使用 UTF-8，改为跟随环境。
- 服务器会跟随 response 的 characterEncoding。
- 浏览器会跟随 response 头 Content-Type 中的 charset 字段。
- 如果服务器未指定编码则使用默认的 ISO-8859-1
- 浏览器如果无法获取 charset 那么使用无参数的 TextDecoder 来解析传回来的二进制数据。
- DatawayFinder 暂时废弃。
- [Issue I1KB31](https://gitee.com/zycgit/hasor/issues/I1KB31) 支持 多个服务聚合在一起，用网关来转发。原来在dataway的UI中接口地址拼接问题已经修复。
- [Issue I1M32H](https://gitee.com/zycgit/hasor/issues/I1M32H) Dataway 可以代码上通过 GlobalConfig 修改默认配置。例如：Structure模板支持灵活配置
- [Issue 62](https://github.com/zycgit/hasor/issues/62) 从 4.2.2 开始，已经发布的 API 接口可以修改 METHOD 了。
- [Issue I1M32H](https://gitee.com/zycgit/hasor/issues/I1M32H) 4.2.2开始后端可以统一定义一系列默认配置项了。
- [Issue I1VNEH](https://gitee.com/zycgit/hasor/issues/I1VNEH) 编辑模式下现在允许修改 API 的 Method 了
- [Issue I1Z3IW](https://gitee.com/zycgit/hasor/issues/I1Z3IW) Dataway界面的 header配置参数直接放在 http协议的请求头中
- [PR 79](https://github.com/zycgit/hasor/pull/79) 优化 HASOR_DATAQL_DATAWAY_API_URL 路径如果设为 / ，UI的路径也会被 InterfaceApiFilter 当作API拦截（cenzhongyuan <a71z@qq.com>）
- [Issue 76](https://github.com/zycgit/hasor/issues/76) 重新实现了 Nacos 作为存储的逻辑，之前实现太复杂容易出错。
- dataway nacos 重新实现，之前的逻辑太过复杂。
- nacos 的配置信息从 环境变量下沉到 settins，但保留环境变量配置模式。
- `HASOR_DATAQL_DATAWAY_NACOSDAL_SHARD_MAX`、`HASOR_DATAQL_DATAWAY_NACOSDAL_HISTORY` 两个配置项不再有效。
- 注意 nacos 作为存储并不适用于 频繁修改 API 的场景，在操作频繁的情况下可能会突破 `HASOR_DATAQL_DATAWAY_NACOSDAL_API_MAX_SIZE` 限制。 好的方案是选择 db 作为存储。
- Dataway 前后端不再处理任何编码
- 数据从 dataway 输出到 浏览器采用 response 中指定的编码来处理。
- 如果没有指定编码则采用默认的 iso-8859-1。
- 前端代码中通过解析 response 响应头 Content-Type 中的 charset 字段来确定使用的解码类型。
- 如果没有 charset 那么用默认的 new TextDecoder() 来进行解码。
- hasor-dataql Finder 使用机制优化，方便后面 import 和第三方 ioc 集成。
- [Issue 13](https://github.com/zycgit/hasor/issues/13) 提供局部 Hint 能力，通过局部 Hint 切换数据源不在是梦了

## 修复
- [Issue I1Z1XF](https://gitee.com/zycgit/hasor/issues/I1Z1XF) 修复4.2.0 dataway api Disable 后还是能访问到Disable的API
- [Issue I2CGAV](https://gitee.com/zycgit/hasor/issues/I2CGAV) 修复 dataway是否可以根据发布接口的接口方法进行控制。
- [Issue I1UERS](https://gitee.com/zycgit/hasor/issues/I1UERS) 修复两个数值相减错误
- [Issue I22MZW](https://gitee.com/zycgit/hasor/issues/I22MZW) 修复三元运算符操作结果错误
- [Issue 78](https://github.com/zycgit/hasor/issues/78) 优化 `Cannot resolve plugin net.hasor:dataql-maven-plugin:xxxx` 的问题
