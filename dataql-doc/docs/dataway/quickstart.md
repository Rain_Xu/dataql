---
id: quickstart
sidebar_position: 2
---
# 快速上手

<br/>《<a href="https://my.oschina.net/ta8210/blog/3234639" target="_blank">Dataway 让 Spring Boot 不再需要 Controller、Service、DAO、Mapper</a>》
<br/>《<a href="https://my.oschina.net/ta8210/blog/3236659" target="_blank">Dataway 配置数据接口时和前端进行参数对接</a>》
<br/>《<a href="https://my.oschina.net/ta8210/blog/3277320" target="_blank">通过 Dataway 配置一个带有分页查询的接口</a>》
<br/>《<a href="https://my.oschina.net/ta8210/blog/4275216" target="_blank">完美兼容老项目！Dataway 返回结构的全面控制</a>》
<br/>《<a href="https://my.oschina.net/ta8210/blog/4293622" target="_blank">Dataway 整合 Swagger2，让 API 管理更顺畅</a>》
<br/>《<a href="https://my.oschina.net/ta8210/blog/4300558" target="_blank">Dataway header 传参</a>》
<br/>《<a href="https://gitee.com/clougence/hasor-examples.git" target="_blank">例子工程</a>》
