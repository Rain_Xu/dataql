---
id: v4.1.12
title: v4.1.12 (2020-07-22)
---

# v4.1.12 (2020-07-22)

## 优化
- [Issue I1N14E](https://gitee.com/zycgit/hasor/issues/I1N14E) 增加 not found api 的情况下，日志层面打印出具体的API是哪个。

## 修复
- [Issue I1NF1A](https://gitee.com/zycgit/hasor/issues/I1NF1A) 修复 4.1.11 新接口创建之后疑似不会正常 跳转到 编辑页面，但是接口已经创建完毕的问题。
- [Issue I1N355](https://gitee.com/zycgit/hasor/issues/I1N355) 修复 4.1.11 版本在接口编辑页面需要点击两次执行，result 才能正常显示的问题。
