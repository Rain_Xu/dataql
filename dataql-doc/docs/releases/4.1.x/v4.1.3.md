---
id: v4.1.3
title: v4.1.3 (2020-04-13)
---

# v4.1.3 (2020-04-13)

## 新增
- 新增 Dataway 框架
- dataway 通过数据库探测机制来实现确定 SQL 执行方案。
- DataQL 增加可以构建多个独立的环境。其中 dataway 使用独立的环境。
- DataQL 新增 SQL 代码片段执行器，开启 DataQL + SQL 模式。支持分页模式，并兼容多种数据库。
- DataQL `FragmentProcess` 接口新增批量处理能力。
- 增加完成事物函数库，完整支持 7种事务传播属性。
- 增加 web 相关的 函数库。
- 增加 加密解密 udf 工具。

## 优化
- DataQL 语法解析器新增支持 标识符可以通过 `` 来囊括特殊字符例如：`+、-、*、/` 等符号
- DataQL `QueryApiBinder` 的 `bindFinder` 支持 Supplier了。
- `mapjoin` 函数名改为 `mapJoin`。
